import React from "react";
import LoadingOverlay from "react-loading-overlay";

const Loader = (props) => {
  return (
    <LoadingOverlay
      active={props.isActive}
      spinner
      styles={{
        overlay: (base) => ({
          ...base,
          background: "rgba(255, 255, 255, 0.5)"
        }),
        wrapper: (base) => ({
          ...base,
          width: "fill-available",
          height: "fill-available"
        }),
        spinner: (base) => ({
          ...base,
          width: "60px",
          "& svg circle": {
            stroke: "#00b5c3"
          }
        })
      }}
    >
      {props.children}
    </LoadingOverlay>
  );
};

export default Loader;
