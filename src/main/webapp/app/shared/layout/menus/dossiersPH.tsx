import React from 'react';
import { translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';
import DossierPH from 'app/entities/dossierPH';

export const DossierPHMenu = () => (
  <NavDropdown
    icon="folder-open"
    name={translate('global.menu.entities.dossierPharmacie')}
    id="dossierPH-menu"
    data-cy="dossierPH"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <DossierPH />
  </NavDropdown>
);
