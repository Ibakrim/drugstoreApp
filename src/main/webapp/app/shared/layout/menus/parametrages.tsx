import React from 'react';
import { translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';
import Parametrage from 'app/entities/parametrage';

export const ParametrageMenu = () => (
  <NavDropdown
    icon="wrench"
    name={translate('global.menu.parametrage.main')}
    id="parametrage-menu"
    data-cy="parametrage"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <Parametrage />
  </NavDropdown>
);
