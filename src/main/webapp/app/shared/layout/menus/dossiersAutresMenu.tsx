import DossierAutresMenu from 'app/entities/dossierAutres';
import React from 'react';
import { translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const DossiersAutresMenu = () => (
  <NavDropdown
    icon="folder"
    name={translate('global.menu.entities.dossierAutre')}
    id="dossierAutres-menu"
    data-cy="dossierAutres"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <DossierAutresMenu />
  </NavDropdown>
);
