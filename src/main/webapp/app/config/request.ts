import { ACCESS_TOKEN_PARAM_NAME } from './constants';
import { Storage } from 'react-jhipster';

export const getToken = () => {
  const token = Storage.local.get('jhi-authenticationToken') || Storage.session.get('jhi-authenticationToken');
  //return localStorage.getItem(ACCESS_TOKEN_PARAM_NAME);
  return token;
};

export const getTokenForParam = () => {
  return `Bearer ${getToken()}`;
};
