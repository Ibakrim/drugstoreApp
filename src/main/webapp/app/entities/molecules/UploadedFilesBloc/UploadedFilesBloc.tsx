import * as React from 'react';

import DeleteIcon from '@material-ui/icons/Delete';
import DownloadIcon from '@material-ui/icons/CloudDownload';

import {
  StyledUploadedDocuments,
  StyledUploadedDocumentTile,
  StyledUpload,
  StyledDownloadIcon,
  StyledFile,
  StyledNameFile,
  StyledSizeFile,
  StyledGarbageIcon,
  StyledBackgroundIcon,
} from './UploadedFilesBloc.style';

import { IDocument } from 'app/shared/model/document.model';
import { Translate } from 'react-jhipster';
import { BYTE_TO_MB_UNIT } from 'app/config/constants';
import { MouseEventHandler, useEffect } from 'react';
export interface Props {
  uploadedFiles: IDocument[];
  deleteFile?: (index: number) => void;
  className?: string;
  downloadFile?: (index: number) => void;
}
const UploadedFilesBloc: React.FunctionComponent<Props> = ({ deleteFile, uploadedFiles, className, downloadFile }) => {
  return (
    <StyledUploadedDocuments className={className}>
      {uploadedFiles.map((file, index) => (
        <StyledUploadedDocumentTile key={file.downloadPath} className={className}>
          <StyledUpload>
            {file.downloadPath &&
              (downloadFile && file.id != 0 ? (
                <a
                  className="download-link"
                  onClick={() => {
                    downloadFile(index);
                  }}
                  target="_BLANK"
                >
                  <StyledDownloadIcon>
                    <DownloadIcon />
                  </StyledDownloadIcon>
                  <StyledBackgroundIcon />
                </a>
              ) : (
                <a className="download-link" target="_BLANK" href={file.downloadPath}>
                  <StyledDownloadIcon>
                    <DownloadIcon />
                  </StyledDownloadIcon>
                  <StyledBackgroundIcon />
                </a>
              ))}

            <StyledFile isFileWithSize={file.documentSize != null && file.documentSize != 0}>
              <StyledNameFile>{file.name}</StyledNameFile>
              {file.documentSize != null && file.documentSize != 0 && (
                <StyledSizeFile>
                  {(file.documentSize / BYTE_TO_MB_UNIT).toFixed(2)}
                  <Translate contentKey="drugStoreApp.document.fileSizeUnit">file size</Translate>
                </StyledSizeFile>
              )}
            </StyledFile>
          </StyledUpload>

          {deleteFile && (
            <StyledGarbageIcon
              key={file.downloadPath}
              onClick={e => {
                deleteFile(index);
              }}
            >
              <DeleteIcon />
            </StyledGarbageIcon>
          )}
        </StyledUploadedDocumentTile>
      ))}
    </StyledUploadedDocuments>
  );
};

export default UploadedFilesBloc;
