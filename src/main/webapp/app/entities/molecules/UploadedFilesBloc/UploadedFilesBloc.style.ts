import styled from 'styled-components';
import { colorPalette, fontSize, fontWeight, getSpacing } from '../../../stylesheet';

interface Props {
  isFileWithSize?: boolean;
}

export const StyledUploadedDocuments = styled.div`
  display: flex;
  margin-top: ${getSpacing(8)};
  justify-content: space-between;
  &.space-around {
    justify-content: start;
    flex-wrap: wrap;
  }
  &.max-width {
    max-width: 100%;
  }
  &.percent-width {
    margin-top: 0px;
    width: 49%;
    margin-left: 45px;
  }
  &.percent-width-margin {
    margin-top: 0px;
    width: 49%;
  }
  &.no-margin {
    margin-top: 0;
  }
`;

export const StyledUploadedDocumentTile = styled.div`
  display: flex;
  width: ${getSpacing(96)};
  height: ${getSpacing(20)};
  border-radius: ${getSpacing(1)};
  box-shadow: 0 2px 9px 0 rgba(0, 0, 0, 0.07);
  justify-content: space-between;
  align-items: center;
  padding: ${getSpacing(3)};
  &:last-child {
    margin-bottom: ${getSpacing(6)};
  }
  &.space-around {
    width: 45%;
    margin: ${getSpacing(2)};
  }
  &.large-width {
    width: 48%;
    margin: ${getSpacing(1)};
  }
  &.max-width {
    padding: 0;
    margin-bottom: 0;
    width: 100%;
  }
  &.no-margin {
    margin-bottom: 0;
  }
`;
export const StyledUpload = styled.div`
  display: flex;
  justify-content: flex-start;
  padding-left: ${getSpacing(3)};
  .download-link {
    display: flex;
  }
`;
export const StyledBackgroundIcon = styled.div`
  opacity: 0.15;
  border-radius: ${getSpacing(4)};
  background-color: ${colorPalette.blueCiel};
  width: calc(${getSpacing(7)} + 2px);
  height: calc(${getSpacing(7)} + 2px);
  margin-right: ${getSpacing(3)};
`;
export const StyledDownloadIcon = styled.div`
  width: 0;
  position: relative;
  left: 3px;
  top: 3px;
`;
export const StyledFile = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: ${getSpacing(1)};
  ${({ isFileWithSize }: Props) => (isFileWithSize === true ? 'text-align: left' : 'align-self: center;')}
`;

export const StyledNameFile = styled.div`
  color: ${colorPalette.grey};
  font-size: ${fontSize.small};
  font-weight: ${fontWeight.bold};
`;
export const StyledSizeFile = styled.div`
  color: ${colorPalette.blue};
  font-size: ${fontSize.xsmall};
  margin-left: ${getSpacing(1)};
  margin-top: ${getSpacing(2)};
`;

export const StyledGarbageIcon = styled.div`
  margin-right: ${getSpacing(3)};
`;
