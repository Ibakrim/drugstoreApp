import React from 'react';
import { Translate } from 'react-jhipster';

import MenuItem from 'app/shared/layout/menus/menu-item';

const DossierPH = () => {
  return (
    <>
      <MenuItem icon="folder-open" to="/dossier-pharmacie">
        <Translate contentKey="global.menu.entities.dossierPharmacie">Dossiers Pharmacies</Translate>
      </MenuItem>
      <MenuItem icon="user-secret" to="/commission">
        <Translate contentKey="global.menu.entities.commission">Commissions</Translate>
      </MenuItem>
    </>
  );
};

export default DossierPH;
