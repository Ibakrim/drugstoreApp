import React from 'react';
import { Translate } from 'react-jhipster';

import MenuItem from 'app/shared/layout/menus/menu-item';

const DossierAutresMenu = () => {
  return (
    <>
      <MenuItem icon="folder" to="/dossier-autre">
        <Translate contentKey="global.menu.entities.dossierAutre">Dossiers Autres PLRs</Translate>
      </MenuItem>
    </>
  );
};

export default DossierAutresMenu;
