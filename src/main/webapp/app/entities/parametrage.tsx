import React from 'react';
import { Translate } from 'react-jhipster';

import MenuItem from 'app/shared/layout/menus/menu-item';

const Parametrage = () => {
  return (
    <>
      <MenuItem icon="building" to="/local">
        <Translate contentKey="global.menu.parametrage.local">Locaux</Translate>
      </MenuItem>
      <MenuItem icon="map-marker" to="/zone">
        <Translate contentKey="global.menu.parametrage.zone">Zones</Translate>
      </MenuItem>
    </>
  );
};

export default Parametrage;
