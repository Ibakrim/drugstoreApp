import axios from 'axios';
import { createAsyncThunk, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { IQueryParams, createEntitySlice, EntityState, serializeAxiosError } from 'app/shared/reducers/reducer.utils';
import { IDossierPharmacie, defaultValue, IDossierPharmacieWithFiles } from 'app/shared/model/dossier-pharmacie.model';
import { getBearerToken } from './services';
import { IDocument } from 'app/shared/model/document.model';
import { AxiosResponse } from 'axios';

const initialState: EntityState<IDossierPharmacie> = {
  loading: false,
  errorMessage: null,
  entities: [],
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

const apiUrl = 'api/dossier-pharmacies';

// Actions

export const getEntities = createAsyncThunk('dossierPharmacie/fetch_entity_list', async ({ page, size, sort }: IQueryParams) => {
  const requestUrl = `${apiUrl}?cacheBuster=${new Date().getTime()}`;
  return axios.get<IDossierPharmacie[]>(requestUrl);
});

export const getEntity = createAsyncThunk(
  'dossierPharmacie/fetch_entity',
  async (id: string | number) => {
    const requestUrl = `${apiUrl}/${id}`;
    return axios.get<IDossierPharmacie>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

const axiosConfig = {
  responseType: 'json',
  timeout: 5000,
  headers: { Authorization: getBearerToken() },
};
const apiDefinition = () => Promise<AxiosResponse<Blob>>;

export const getFileFromDownloadURL = createAsyncThunk(
  'dossierPharmacie/fetch_file',
  async (entity: IDocument) => {
    console.log('here in getFileFromDownloadURL  eeeee');

    const requestUrl = `${entity.downloadPath}/DOSSIER_PHARMACIE_DOCUMENT`;
    console.log('here in getFileFromDownloadURL  eeeee ' + requestUrl);
    /*const result= await axios.get(requestUrl,{responseType: "blob",timeout: 5000,headers: { Authorization: getBearerToken() }});

    console.log( result);
    //return URL.revokeObjectURL(url);
    const url = window.URL.createObjectURL(new Blob([result.data]));
  const link = document.createElement('a');
  link.href = url;
  console.log( url);
  link.setAttribute('download', "currentFileInfo");
  document.body.appendChild(link);
  return link.click();*/

    await axios({
      url: requestUrl,
      method: 'GET',
      responseType: 'blob',
      timeout: 5000,
      headers: { Authorization: getBearerToken() },
    }).then(response => {
      console.log(response);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const contentDisposition = response.headers['content-disposition'];
      let fileName = 'unknown';
      if (contentDisposition) {
        const fileNameMatch = contentDisposition.match(/filename="(.+)"/);
        if (fileNameMatch.length === 2) fileName = fileNameMatch[1];
      }
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
    });
  },
  { serializeError: serializeAxiosError }
);

export const createEntity = createAsyncThunk(
  'dossierPharmacie/create_entity',
  async (entity: IDossierPharmacie, thunkAPI) => {
    const result = await axios.post<IDossierPharmacie>(apiUrl, cleanEntity(entity));
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const createEntityWithFiles = createAsyncThunk(
  'dossierPharmacie/create_entity',
  async (entity: IDossierPharmacieWithFiles, thunkAPI) => {
    const apiUrlWithFiles = 'api/dossier-pharmacies-with-files';

    let formData = new FormData();

    for (let file of entity.files) {
      formData.append('files', file);
    }

    formData.append(
      'dossierPharmacie',
      new Blob([JSON.stringify(cleanEntity(entity.entity))], {
        type: 'application/json',
      })
    );
    const result = await axios.post(apiUrlWithFiles, formData);
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const updateEntityWithFiles = createAsyncThunk(
  'dossierPharmacie/update_entity',
  async (entity: IDossierPharmacieWithFiles, thunkAPI) => {
    const apiUrlWithFiles = `api/${entity.entity.id}/update-with-files`;
    let formData = new FormData();

    for (let file of entity.files) {
      formData.append('files', file);
    }

    formData.append(
      'dossierPharmacie',
      new Blob([JSON.stringify(cleanEntity(entity.entity))], {
        type: 'application/json',
      })
    );
    const result = await axios.put(apiUrlWithFiles, formData);
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const updateEntity = createAsyncThunk(
  'dossierPharmacie/update_entity',
  async (entity: IDossierPharmacie, thunkAPI) => {
    const result = await axios.put<IDossierPharmacie>(`${apiUrl}/${entity.id}`, cleanEntity(entity));
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const partialUpdateEntity = createAsyncThunk(
  'dossierPharmacie/partial_update_entity',
  async (entity: IDossierPharmacie, thunkAPI) => {
    const result = await axios.patch<IDossierPharmacie>(`${apiUrl}/${entity.id}`, cleanEntity(entity));
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const deleteEntity = createAsyncThunk(
  'dossierPharmacie/delete_entity',
  async (id: string | number, thunkAPI) => {
    const requestUrl = `${apiUrl}/${id}`;
    const result = await axios.delete<IDossierPharmacie>(requestUrl);
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

// slice

export const DossierPharmacieSlice = createEntitySlice({
  name: 'dossierPharmacie',
  initialState,
  extraReducers(builder) {
    builder
      .addCase(getEntity.fulfilled, (state, action) => {
        state.loading = false;
        state.entity = action.payload.data;
      })
      .addCase(deleteEntity.fulfilled, state => {
        state.updating = false;
        state.updateSuccess = true;
        state.entity = {};
      })
      .addMatcher(isFulfilled(getEntities), (state, action) => {
        const { data } = action.payload;

        return {
          ...state,
          loading: false,
          entities: data,
        };
      })
      .addMatcher(isFulfilled(createEntity, updateEntity, partialUpdateEntity), (state, action) => {
        state.updating = false;
        state.loading = false;
        state.updateSuccess = true;
        state.entity = action.payload.data;
      })
      .addMatcher(isPending(getEntities, getEntity), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.loading = true;
      })
      .addMatcher(isPending(createEntity, updateEntity, partialUpdateEntity, deleteEntity), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.updating = true;
      });
  },
});

export const { reset } = DossierPharmacieSlice.actions;

// Reducer
export default DossierPharmacieSlice.reducer;
