import { getTokenForParam } from 'app/config/request';
import { IDocument } from 'app/shared/model/document.model';
import memoizeOne from 'memoize-one';
import * as Yup from 'yup';

export const getDownloadAcknowledgmentUrl = memoizeOne(
  (downloadpathUrl: string): string => `${downloadpathUrl}DOSSIER_PHARMACIE_DOCUMENT?token=${getTokenForParam()}`
);

export const convertDocumentToUploadedFile = (documentList: Array<IDocument>): IDocument[] => {
  documentList.length > 0 && console.log('document.downloadPath ' + getDownloadAcknowledgmentUrl(documentList[0].downloadPath));
  console.log(documentList);
  return documentList.map(document => ({
    id: document.id,
    name: document.name,
    mimeType: document.mimeType,
    downloadPath: getDownloadAcknowledgmentUrl(document.downloadPath),
    thumbnailDownloadPath: getDownloadAcknowledgmentUrl(document.thumbnailDownloadPath),
    thumbnailPath: getDownloadAcknowledgmentUrl(document.thumbnailPath),
    uploadDate: document.uploadDate,
    size: document.documentSize,
  }));
};

export const getBearerToken = () => {
  var authToken = localStorage.getItem('jhi-authenticationToken') || sessionStorage.getItem('jhi-authenticationToken');
  if (authToken) {
    authToken = JSON.parse(authToken);
    return `Bearer ${authToken}`;
  }
  return null;
};

export const validationSchema = () =>
  Yup.object().shape({
    dateDepot: Yup.date().nullable(true),
    dateDerniereModif: Yup.date().nullable(true).min(Yup.ref('dateDepot'), "la date d'entrée ne peut pas etre avant la date de sortie"),
    numero: Yup.number().typeError('Ce champ doit etre un nombre').positive('Ce champ doit etre un nombre positif').nullable(true),
  });

export const initFormikValues = {
  id: '',
  numeroAffaire: '',
  tiers: '',
  dateRemonteeAffaire: '',
  nomClient: '',
  societeGestionNom: '',
  marqueVehicule: '',
  numeroImmatriculation: '',
  isBlocked: '',
  statutAffaire: '',
  cinRc: '',
  nombreImpayes: '',
  montantCreance: '',
  montantImpaye: '',
  dateDemandeRecuperation: '',
  isVehiculeLocalise: '',
  dateLocalisation: '',
  sortDemandeRecuperation: '',
  isVehiculeRecupere: '',
  dateRecuperation: '',
  recuperateur: '',
  natureRecuperation: '',
  ville: '',
  regionRecuperation: '',
  prestataireRecuperation: '',
  lieuRecuperation: '',
  isPresenceHuissier: '',
  nomHuissier: '',
  lieuPremierStockage: '',
  dateEntree: '',
  dateSortie: '',
  coutParking: '',
  sortVehicule: '',
  lieuxStockage: null,
  etatCarrosserie: '',
  etatRoues: '',
  commentaireEtat: '',
  propositionsVente: null,
  sortProcedural: '',
  sortJugement: '',
  numExecution: '',
  qualificationDossier: '',
  motifQualificationDossier: '',
  isRecuperationTribunal: '',
  isPublicationJournaux: '',
  isMiseExpertise: '',
  datePrevueVente: '',
  cinAcheteur: '',
  parkingTotalJour: '',
  lieuStockageActuel: '',
  fraisRecuperation: {
    numeroFactureRec: '',
    recuperation: '',
    huissierRecuperation: '',
    remorquage: '',
    rapatriement: '',
    gardiennage: '',
    bonus: '',
    totalRec: '',
  },
  fraisVente: {
    numeroFactureVt: '',
    huissierVente: '',
    huissierVenteReport: '',
    publicite: '',
    expert: '',
    totalVt: '',
  },
};
