import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { isNumber, Translate, translate, ValidatedBlobField, ValidatedField, ValidatedForm, ValidatedInput } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IDocument } from 'app/shared/model/document.model';
import { getEntities as getDocuments } from 'app/entities/document/document.reducer';
import { IUtilisateur } from 'app/shared/model/utilisateur.model';
import { getEntities as getUtilisateurs } from 'app/entities/utilisateur/utilisateur.reducer';
import { ILocal } from 'app/shared/model/local.model';
import { getEntities as getLocals } from 'app/entities/local/local.reducer';
import { IDossierPharmacie, IDossierPharmacieWithFiles } from 'app/shared/model/dossier-pharmacie.model';
import {
  getEntity,
  updateEntity,
  createEntity,
  reset,
  createEntityWithFiles,
  updateEntityWithFiles,
  getFileFromDownloadURL,
} from './dossier-pharmacie.reducer';
import { SIZE_LIMIT_FILE } from 'app/config/constants';
import UploadedFilesBloc from '../molecules/UploadedFilesBloc';
import { convertDocumentToUploadedFile, validationSchema } from './services';
import Loader from 'app/shared/Loader';
import { Field, Form, Formik } from 'formik';
//import { Box, Grid, Button, IconButton, Paper, Toolbar, Typography, makeStyles, MenuItem, SvgIcon } from "@material-ui/core";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { TextField } from 'formik-material-ui';
import { Button, Col, FormGroup, Input, Label, Row } from 'reactstrap';

export const DossierPharmacieUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const users = useAppSelector(state => state.userManagement.users);
  const documents = useAppSelector(state => state.document.entities);
  const utilisateurs = useAppSelector(state => state.utilisateur.entities);
  const locals = useAppSelector(state => state.local.entities);
  const dossierPharmacieEntity = useAppSelector(state => state.dossierPharmacie.entity);
  const loading = useAppSelector(state => state.dossierPharmacie.loading);
  const updating = useAppSelector(state => state.dossierPharmacie.updating);
  const updateSuccess = useAppSelector(state => state.dossierPharmacie.updateSuccess);

  const handleClose = () => {
    navigate('/dossier-pharmacie');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getUsers({}));
    dispatch(getDocuments({}));
    dispatch(getUtilisateurs({}));
    dispatch(getLocals({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.dateDepot = convertDateTimeToServer(values.dateDepot);
    values.dateDerniereModif = convertDateTimeToServer(values.dateDerniereModif);

    const entity = {
      ...dossierPharmacieEntity,
      ...values,
      documentLists: mapIdList(values.documentLists),
      user: users.find(it => it.id.toString() === values.user.toString()),
      utilisateur: utilisateurs.find(it => it.id.toString() === values.utilisateur.toString()),
      local: locals.find(it => it.id.toString() === values.local.toString()),
    };
    const entityUpdated = {
      ...dossierPharmacieEntity,
      ...values,
      documentLists: mapIdList(convertDocumentToUploadedFile(filesFromDB).map(e => e.id.toString())),
      user: users.find(it => it.id.toString() === values.user.toString()),
      utilisateur: utilisateurs.find(it => it.id.toString() === values.utilisateur.toString()),
      local: locals.find(it => it.id.toString() === values.local.toString()),
    };

    const entityWithFiles: IDossierPharmacieWithFiles = { files: files, entity: entity };
    const entityUpdatedWithFiles: IDossierPharmacieWithFiles = { files: files, entity: entityUpdated };
    console.log('here filefilesToUPload:');
    console.log(files);
    if (isNew) {
      if (files) {
        dispatch(createEntityWithFiles(entityWithFiles));
      } else {
        dispatch(createEntity(entity));
      }
    } else {
      if (files || filesFromDB.length !== dossierPharmacieEntity.documentList.length) {
        console.log('here in updateEntityWithFiles');
        dispatch(updateEntityWithFiles(entityUpdatedWithFiles));
      } else {
        console.log('here in normal update');
        dispatch(updateEntity(entity));
      }
    }
  };
  const [file, setFile] = useState<File>(null);
  const [files, setFiles] = useState<File[]>([]);
  const [filesFromDB, setFilesFromDB] = useState<File[]>([]);
  const handleFileChoice = (event: React.ChangeEvent): void => {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    const selectedFile = target.files ? target.files[0] : null;
    target.value = '';
    if (selectedFile) {
      const typeUpload = selectedFile.type.match(/pdf/) ? SIZE_LIMIT_FILE.PDF : SIZE_LIMIT_FILE.PICTURE;
      console.log('size   :' + selectedFile.size);
      if (selectedFile.size < typeUpload.size) {
        files.push(selectedFile);
        setFiles(files);
        setFile(selectedFile);
      }
    }
  };

  console.log('files :');
  console.log(files);
  console.log('filesFromDB :');
  console.log(filesFromDB);

  useEffect(() => {
    console.log('1 here in useeffect ');
    if (dossierPharmacieEntity?.documentLists) {
      console.log('2 here in useeffect ');
      setFilesFromDB(dossierPharmacieEntity.documentLists);
    }
  }, [dossierPharmacieEntity]);

  const defaultValues = () =>
    isNew
      ? {
          dateDepot: displayDefaultDateTime(),
          dateDerniereModif: displayDefaultDateTime(),
          files: [],
        }
      : {
          ...dossierPharmacieEntity,
          dateDepot: convertDateTimeFromServer(dossierPharmacieEntity.dateDepot),
          dateDerniereModif: convertDateTimeFromServer(dossierPharmacieEntity.dateDerniereModif),
          user: dossierPharmacieEntity?.user?.id,
          documentLists: dossierPharmacieEntity?.documentLists?.map(e => e.id.toString()),
          utilisateur: dossierPharmacieEntity?.utilisateur?.id,
          local: dossierPharmacieEntity?.local?.id,
        };

  const deleteFile = (index: number): void => {
    setFile(files[index]);
    files.splice(index, 1);
    setFiles(files);
  };
  const deleteFileInUpdateMode = (index: number): void => {
    if (index < filesFromDB.length) {
      setFile(filesFromDB[index]);
      let documentsAfterDeletion = filesFromDB.filter((file, key) => key !== index);
      setFilesFromDB(documentsAfterDeletion);
    } else {
      let documentListFromBDLength = filesFromDB.length;
      setFile(files[index - documentListFromBDLength]);
      files.splice(index - documentListFromBDLength, 1);
      setFiles(files);
    }
  };

  const downloadFile = (index: number): void => {
    if (index < filesFromDB.length) {
      let fileToDownload: IDocument = filesFromDB[index];
      console.log('here in download file eeeee yes');
      console.log(fileToDownload);
      dispatch(getFileFromDownloadURL(fileToDownload));
    } else {
      console.log('here in download file eeeee not');
      let documentListFromBDLength = filesFromDB.length;
      let fileToDownload: IDocument = filesFromDB[index - documentListFromBDLength];
      dispatch(getFileFromDownloadURL(fileToDownload));
    }
  };

  const convertFileToUploadedFileWithSize = (files: File[], documentLists: IDocument[]): IDocument[] => {
    const uploadedFileWithSize: IDocument[] = [];
    files.map(file => {
      uploadedFileWithSize.push({
        id: 0,
        name: file.name,
        mimeType: '',
        downloadPath: URL.createObjectURL(file),
        thumbnailDownloadPath: URL.createObjectURL(file),
        thumbnailPath: URL.createObjectURL(file),
        uploadDate: new Date(),
        documentSize: file.size,
      });
    });

    return documentLists.concat(uploadedFileWithSize);
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="drugStoreApp.dossierPharmacie.home.createOrEditLabel" data-cy="DossierPharmacieCreateUpdateHeading">
            <Translate contentKey="drugStoreApp.dossierPharmacie.home.createOrEditLabel">Create or edit a DossierPharmacie</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="dossier-pharmacie-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.numero')}
                id="dossier-pharmacie-numero"
                name="numero"
                data-cy="numero"
                type="text"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.dateDepot')}
                id="dossier-pharmacie-dateDepot"
                name="dateDepot"
                data-cy="dateDepot"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.nature')}
                id="dossier-pharmacie-nature"
                name="nature"
                data-cy="nature"
                type="text"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.statut')}
                id="dossier-pharmacie-statut"
                name="statut"
                data-cy="statut"
                type="text"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.dateDerniereModif')}
                id="dossier-pharmacie-dateDerniereModif"
                name="dateDerniereModif"
                data-cy="dateDerniereModif"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.commentaire')}
                id="dossier-pharmacie-commentaire"
                name="commentaire"
                data-cy="commentaire"
                type="text"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.nom')}
                id="dossier-pharmacie-nom"
                name="nom"
                data-cy="nom"
                type="text"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.prenom')}
                id="dossier-pharmacie-prenom"
                name="prenom"
                data-cy="prenom"
                type="text"
              />
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.telephone')}
                id="dossier-pharmacie-telephone"
                name="telephone"
                data-cy="telephone"
                type="text"
              />
              <FormGroup>
                <Label for="document">{translate('drugStoreApp.dossierPharmacie.documentLabel')}</Label>
                <Input
                  label="file"
                  type="file"
                  multiple
                  id="dossier-pharmacie-document"
                  accept=".jpg,.png,.pdf"
                  name="document"
                  data-cy="document"
                  onChange={handleFileChoice}
                />
                {isNew
                  ? files && <UploadedFilesBloc className="space-around" deleteFile={index => deleteFile(index)} uploadedFiles={files} />
                  : filesFromDB && (
                      <UploadedFilesBloc
                        className="space-around"
                        deleteFile={index => deleteFileInUpdateMode(index)}
                        uploadedFiles={convertFileToUploadedFileWithSize(files, convertDocumentToUploadedFile(filesFromDB))}
                        downloadFile={index => downloadFile(index)}
                      />
                    )}
              </FormGroup>
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.cIN')}
                id="dossier-pharmacie-cIN"
                name="cIN"
                data-cy="cIN"
                type="text"
              />
              <ValidatedField
                id="dossier-pharmacie-user"
                name="user"
                data-cy="user"
                label={translate('drugStoreApp.dossierPharmacie.user')}
                type="select"
              >
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                label={translate('drugStoreApp.dossierPharmacie.documentList')}
                id="dossier-pharmacie-documentList"
                data-cy="documentList"
                type="select"
                multiple
                name="documentLists"
              >
                <option value="" key="0" />
                {documents
                  ? documents.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="dossier-pharmacie-utilisateur"
                name="utilisateur"
                data-cy="utilisateur"
                label={translate('drugStoreApp.dossierPharmacie.utilisateur')}
                type="select"
              >
                <option value="" key="0" />
                {utilisateurs
                  ? utilisateurs.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="dossier-pharmacie-local"
                name="local"
                data-cy="local"
                label={translate('drugStoreApp.dossierPharmacie.local')}
                type="select"
              >
                <option value="" key="0" />
                {locals
                  ? locals.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/dossier-pharmacie" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default DossierPharmacieUpdate;
