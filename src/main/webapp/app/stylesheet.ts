/**
 * App spacing measurement convention
 * Use the getSpacing function below to compute padding and margin
 */
const SPACING_UNIT = 5;
const MEASUREMENT_UNIT = 'px';

/**
 * Do not use directly the colorPalette in your components
 * Create an entry in the colorUsage below instead
 */
export const colorPalette = {
  dark: '#000000',
  grey: '#4a5c7b',
  lighterGrey: '#e6e6e6',
  textGrey: '#9d9da8',
  textLightGrey: '#6b7897',
  lighterGreyBackground: '#f9fafb',
  textGreyLighter: '#b4b7bb',
  lightGrey: '#ededf0',
  lightBlueGrey: '#e7e9ea',
  brightGreyLine: '#d5dbe5',
  brightGrey: '#b0bac9',
  blue: '#1a76f0',
  blueCiel: '#015cff',
  lighterBlue: '#e0eaff',
  green: '#50d2c2',
  darkGreen: '#23554f',
  darkerGreen: '#008294',
  darkerWhite: '#fbfbfc',
  white: '#ffffff',
  red: '#ee0000',
  shadowGrey: '#f4f4f4',
  gray29: '#4a4a4a',
  middleGrey: '#c3c3c3',
  nobel: '#989898',
  darkBlueWithOpacity: '#08162F',
  pinkRed: '#f4365e',
  orange: '#f9ca6b',
  darkOrange: '#a08142',
  lightWhite: '#2d2d2d',
  darkGrey: '#474f58',
  darkerGrey: '#8c96ae',
  lightBlue: '#e9eff4',
  skyBlue: '#4ea1eb',
  mediumBlue: '#2196f3',
  lightBlueWithOpacity: '#c0deff1a',
  lightBlueWithoutOpacity: '#c0deff',
  ultraLightGray: '#ebebeb',
  blackGrey: '#808080',
  blackerGrey: '#090909',
  lightBlackGrey: '#999999',
  blueGrey: '#f5f6f7',
  blackLight: '#343c4b',
  blackBlue: '#2e384d',
  timelineGrey: '#a6afbe',
  waterBlue: '#c3d6ff',
  grey99: '#fcfcfc',
  grey80: '#f8f9fa',
  lightRed: '#fff3f5',
  grey33: '#eaecf1',
  grey44: '#3a4f70',
  grey55: '#9ba8be',
  lighterGreen: '#eafaf8',
  lightgray: '#d6d6d6',
  lighterGreyWithOpacity: '#f9fcff',
  ghostwhite: '#e7e9ea',
  darkCyan: '#008294',
  pineGreen: '#136f7b',
  gray6: '#d6d6d6',
  pineGreenLight: '#d8dce7',
  progressBarGrey: '#eaecf1',
  statisticBorderBlue: '#65a8ff',
  vLightGrey: '#f7f7f7',
  azureLight: '#e8f1fd',
  azure: '#1a76f0',
  lighterAzure: '#E9F1FD',
  darkBlue: '#4050b5',
};

/**
 * Use this dictionnary in your components
 * Define a new key each time you use a colour if it's for a different use
 * Ex: fill, border-color, background-color, color ...
 */
export const colorUsage = {
  mapMarker: colorPalette.waterBlue,
  middleGrey: colorPalette.middleGrey,
  textLightGrey: colorPalette.textLightGrey,
  lighterGreyBackgroundColor: colorPalette.lighterGreyBackground,
  lineBreakGrey: colorPalette.lightBlueGrey,
  primaryTitleColor: colorPalette.gray29,
  white: colorPalette.white,
  headerTextColor: colorPalette.blue,
  titleTextColor: colorPalette.dark,
  subTitleTextColor: colorPalette.textGrey,
  primaryTextColor: colorPalette.grey,
  secondaryTextColor: colorPalette.green,
  chipBackgoundColor: colorPalette.lightGrey,
  primaryButtonColor: colorPalette.white,
  secondaryButtonColor: colorPalette.blue,
  primaryBackgroundColor: colorPalette.blue,
  secondaryBackgroundColor: colorPalette.lighterBlue,
  projectDocumentsBackgroundColor: colorPalette.darkerWhite,
  projectDisabledProcedureColor: colorPalette.timelineGrey,
  projectDocumentsForegroundColor: colorPalette.white,
  chipColor: colorPalette.grey,
  activeChipBackgroundColor: colorPalette.blue,
  activeChipColor: colorPalette.darkerWhite,
  submitButtonBackgroundColor: colorPalette.blue,
  submitButtonText: colorPalette.white,
  steperNumber: colorPalette.white,
  border: colorPalette.red,
  checkboxBorder: colorPalette.textGrey,
  checkboxColor: colorPalette.green,
  navMenuTextColor: colorPalette.textGrey,
  navMenuBackground: colorPalette.white,
  navMenuHoverBackground: colorPalette.darkerWhite,
  disabledSubmitButtonBackgroundColor: colorPalette.grey,
  separatorColor: colorPalette.lightGrey,
  iconButtonColor: colorPalette.textGrey,
  iconLinkColor: colorPalette.blue,
  backButtonBackgroundColor: colorPalette.lightGrey,
  shadowBoxColor: colorPalette.shadowGrey,
  statProjectColor: colorPalette.gray29,
  homeColor: colorPalette.nobel,
  rememberMeText: colorPalette.textGrey,
  createAcountColor: colorPalette.blueCiel,
  invalidDocumentColor: colorPalette.pinkRed,
  notificationDateColor: colorPalette.darkGrey,
  notificationBoxBorderColor: colorPalette.lightBlue,
  notificationFooterButtonColor: colorPalette.mediumBlue,
  homeTitleColor: colorPalette.white,
  modalCloseIcon: colorPalette.ultraLightGray,
  blueBackground: colorPalette.lightBlueWithOpacity,
  inputPlaceholder: 'rgba(0,0,0,0.54)',
  lighGreenBackgroundWithOpacity: 'rgba(80, 210, 194, 0.1)',
  lightRed: colorPalette.lightRed,
  detailText: colorPalette.grey55,
  tileBackground: colorPalette.grey80,
  darkCyanPiafe: colorPalette.darkCyan,
  sheetAdd: colorPalette.darkCyan,
  moreVert: colorPalette.gray6,
  sheetAddBorder: colorPalette.pineGreenLight,
  timeLineStepInProgress: colorPalette.darkerGreen,
  timeLineStepNotStarted: colorPalette.timelineGrey,
  timeLineStepDone: colorPalette.dark,
  questionText: colorPalette.blackerGrey,
  selectButtonBorder: colorPalette.skyBlue,
  darkerGreen: colorPalette.darkerGreen,
  darkBlue: colorPalette.darkBlue,
};

export const fontFamily = {
  title: `'Open Sans', sans-serif`,
  main: `'Open Sans', sans-serif`,
  robotoPiafe: 'Roboto, Arial, Helvetica, sans-serif',
};

export const fontSize = {
  ultralarge: '60px',
  xxxxlarge: '48px',
  xxxlarge: '38px',
  xxlarge: '32px',
  xlarge: '26px',
  large: '21px',
  medium: '18px',
  small: '15px',
  smaller: '14px',
  xsmall: '12px',
  ultraSmall: '10px',
};

export const fontWeight = {
  ultrabold: '900',
  bold: '700',
  medium: '600',
  normal: '400',
};

export const lineHeight = {
  medium: 'normal',
};

export const getSpacing = (multiplier: number): string => `${multiplier * SPACING_UNIT}${MEASUREMENT_UNIT}`;

interface ColorFromStateInterface {
  [key: string]: string;
}

export const colorFromState: ColorFromStateInterface = {
  done: colorPalette.green,
  inProgress: colorPalette.blue,
  notStarted: colorPalette.textGrey,
};
