/**
 *
 */
package com.drugstore.app.utils;

import com.drugstore.app.storage.config.AppConfig;
import java.math.BigDecimal;

/**
 * @author Ibakrim
 *
 */
public class DrugStoreUtils {

    public static AppConfig getAppConfig() {
        return appConfig;
    }

    public static void setAppConfig(AppConfig appConfig) {
        DrugStoreUtils.appConfig = appConfig;
    }

    private static AppConfig appConfig;

    public static String getSchemaHostPort() {
        String result = null;
        result = appConfig.getBaseURL();
        return result;
    }

    public static String getSupportSchemaHostPort() {
        String result = appConfig.getSupportBaseURL();
        return result;
    }

    public static String getSchemaHostPortContext() {
        String result = null;
        result = appConfig.getBaseURL();
        return result;
    }

    public static String getContextPath() {
        String result = null;
        result = appConfig.getContextPath();
        return result;
    }

    public static boolean isNull(Object field) {
        if (field != null) {
            return false;
        }

        return true;
    }

    public static boolean isBlank(String field) {
        if (!field.isEmpty()) {
            return false;
        }

        return true;
    }

    public static boolean isGretterThan(Integer field, int value) {
        if (field <= value) {
            return false;
        }

        return true;
    }

    public static boolean isGretterThan(double d, int value) {
        if (d <= value) {
            return false;
        }

        return true;
    }

    public static boolean isEqual(Object field, Object value) {
        if (!field.equals(value)) {
            return false;
        }

        return true;
    }

    public static boolean isNullOrNegative(BigDecimal i) {
        return (i == null || i.doubleValue() <= 0);
    }
}
