package com.drugstore.app.utils;

public class DrugStoreConstants {

    public static final String ZIP_EXTENSION = ".zip";
    public static final Integer IMAGE_DPI = 300;
}
