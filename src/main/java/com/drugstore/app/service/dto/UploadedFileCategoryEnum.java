package com.drugstore.app.service.dto;

public enum UploadedFileCategoryEnum {
    DOSSIER_PHARMACIE_DOCUMENT,
    DOSSIER_AUTRES_DOCUMENT,
}
