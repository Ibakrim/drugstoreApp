package com.drugstore.app.service.dto;

import com.drugstore.app.domain.DossierAutre;
import com.drugstore.app.domain.DossierPharmacie;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class DocumentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;

    private String mimeType;

    private String path;

    private ZonedDateTime uploadDate;

    private String thumbnailName;

    private String thumbnailPath;

    private String downloadPath;

    private String thumbnailDownloadPath;

    private Long documentSize;

    private Set<DossierPharmacie> dossierPharmacieLists = new HashSet<>();

    private Set<DossierAutre> dossierAutreLists = new HashSet<>();

    public DocumentDTO() {}

    public DocumentDTO(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public DocumentDTO(String name, String mimeType, String path) {
        this.name = name;
        this.mimeType = mimeType;
        this.path = path;
    }

    public DocumentDTO(String name, String mimeType, String path, ZonedDateTime uploadDate) {
        this.name = name;
        this.mimeType = mimeType;
        this.path = path;
        this.uploadDate = uploadDate;
    }

    public DocumentDTO(String name, String mimeType, String path, ZonedDateTime uploadDate, Long documentSize) {
        this.name = name;
        this.mimeType = mimeType;
        this.path = path;
        this.uploadDate = uploadDate;
        this.documentSize = documentSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ZonedDateTime getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(ZonedDateTime uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getThumbnailName() {
        return thumbnailName;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getThumbnailDownloadPath() {
        return thumbnailDownloadPath;
    }

    public void setThumbnailDownloadPath(String thumbnailDownloadPath) {
        this.thumbnailDownloadPath = thumbnailDownloadPath;
    }

    public Long getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(Long documentSize) {
        this.documentSize = documentSize;
    }

    public Set<DossierPharmacie> getDossierPharmacieLists() {
        return dossierPharmacieLists;
    }

    public void setDossierPharmacieLists(Set<DossierPharmacie> dossierPharmacieLists) {
        this.dossierPharmacieLists = dossierPharmacieLists;
    }

    public Set<DossierAutre> getDossierAutreLists() {
        return dossierAutreLists;
    }

    public void setDossierAutreLists(Set<DossierAutre> dossierAutreLists) {
        this.dossierAutreLists = dossierAutreLists;
    }
}
