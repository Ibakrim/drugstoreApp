package com.drugstore.app.service.impl;

import com.drugstore.app.domain.Document;
import com.drugstore.app.repository.DocumentRepository;
import com.drugstore.app.service.IUploadFileService;
import com.drugstore.app.storage.service.IStorageService;
import com.drugstore.app.storage.service.impl.FileSystemStorageService;
import com.drugstore.app.storage.service.impl.ProcessingException;
import com.drugstore.app.utils.DrugStoreUtils;
import com.drugstore.app.web.rest.DocumentResource;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

@Transactional
@Service
public class UploadFileService implements IUploadFileService {

    @Autowired
    protected DocumentRepository documentRepository;

    @Autowired
    private IStorageService storageService;

    @Autowired
    private FileSystemStorageService fileSystemStorageService;

    public Document addUploadedFile(MultipartFile file) {
        try {
            String path = storageService.store(file);
            Document uploadFile = new Document();
            uploadFile.setName(file.getOriginalFilename());
            uploadFile.setPath(path);
            uploadFile.setMimeType(file.getContentType());
            uploadFile.setDocumentSize(file.getSize());
            uploadFile.setUploadDate(new Date());
            uploadFile = this.save(uploadFile);
            return uploadFile;
        } catch (Exception e) {
            throw new ProcessingException("An Exception occured during the add of uploaded file", e);
        }
    }

    @Override
    public Document save(Document document) {
        document = documentRepository.save(document);
        String schemaHostPort = DrugStoreUtils.getSchemaHostPort();
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(schemaHostPort);
        String downloadPath = MvcUriComponentsBuilder
            .fromMethodName(uriComponentsBuilder, DocumentResource.class, "serveCategorizedFile", document.getId(), null)
            .build()
            .getPath();
        String thumbnailDownloadPath = MvcUriComponentsBuilder
            .fromMethodName(uriComponentsBuilder, DocumentResource.class, "serveCategorizedThumbnail", document.getId(), null)
            .build()
            .getPath();
        String schemaHostPortContext = DrugStoreUtils.getSchemaHostPortContext();
        document.setDownloadPath(schemaHostPortContext + downloadPath);
        document.setThumbnailDownloadPath(schemaHostPortContext + thumbnailDownloadPath);
        return documentRepository.save(document);
    }
}
