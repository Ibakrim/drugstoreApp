package com.drugstore.app.service.impl;

import com.drugstore.app.domain.Document;
import com.drugstore.app.domain.DossierPharmacie;
import com.drugstore.app.repository.DocumentRepository;
import com.drugstore.app.repository.DossierPharmacieRepository;
import com.drugstore.app.service.IDossierPharmacieService;
import com.drugstore.app.service.IUploadFileService;
import com.drugstore.app.storage.service.IStorageService;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Transactional
@Service
public class DossierPharmacieService implements IDossierPharmacieService {

    @Autowired
    private IUploadFileService uploadFileService;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private DossierPharmacieRepository dossierPharmacieRepository;

    @Autowired
    private IStorageService storageService;

    @Override
    public DossierPharmacie createDossierPharmacieWithFiles(DossierPharmacie dossierPharmacie, MultipartFile[] files) {
        Set<Document> documentLists = new HashSet<Document>();
        for (MultipartFile file : files) {
            Document doc = uploadFileService.addUploadedFile(file);
            doc = documentRepository.save(doc);
            documentLists.add(doc);
        }
        dossierPharmacie.setDocumentLists(documentLists);
        dossierPharmacie = dossierPharmacieRepository.save(dossierPharmacie);
        return dossierPharmacie;
    }

    @Override
    public DossierPharmacie updateDossierPharmacieWithFiles(
        Long dossierPharmacieId,
        DossierPharmacie dossierPharmacie,
        MultipartFile[] files
    ) {
        DossierPharmacie dossierPharmacieFromDB = dossierPharmacieRepository.findById(dossierPharmacieId).get();
        Set<Document> documentLists = new HashSet<Document>();
        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                Document doc = uploadFileService.addUploadedFile(file);
                doc = documentRepository.save(doc);
                documentLists.add(doc);
            }
        }

        if (dossierPharmacie.getDocumentLists() != null && dossierPharmacie.getDocumentLists().size() > 0) {
            documentLists.addAll(dossierPharmacie.getDocumentLists());
        }

        dossierPharmacie.setDocumentLists(documentLists);

        handleDeleteStoredFiles(dossierPharmacie, dossierPharmacieFromDB);

        dossierPharmacie = dossierPharmacieRepository.save(dossierPharmacie);
        return dossierPharmacie;
    }

    private void handleDeleteStoredFiles(DossierPharmacie dossierPharmacie, DossierPharmacie dossierPharmacieFromDB) {
        if (dossierPharmacieFromDB.getDocumentLists() != null && !dossierPharmacieFromDB.getDocumentLists().isEmpty()) {
            if (dossierPharmacie.getDocumentLists() != null && !dossierPharmacie.getDocumentLists().isEmpty()) {
                if (dossierPharmacie.getDocumentLists().size() != dossierPharmacieFromDB.getDocumentLists().size()) {
                    DossierPharmacie finalDossierPharmacie = dossierPharmacie;
                    dossierPharmacieFromDB
                        .getDocumentLists()
                        .forEach(document -> {
                            if (!finalDossierPharmacie.getDocumentLists().contains(document)) {
                                documentRepository.deleteById(document.getId());
                                storageService.deleteStoredFiles(document);
                            }
                        });
                }
            } else {
                dossierPharmacieFromDB
                    .getDocumentLists()
                    .forEach(document -> {
                        documentRepository.deleteById(document.getId());
                        storageService.deleteStoredFiles(document);
                    });
            }
        }
    }

    @Override
    public void handleFileDelete(DossierPharmacie dossierPharmacie, Document document) {
        Set<Document> documentLists = dossierPharmacie.getDocumentLists();
        documentLists.remove(document);
        dossierPharmacie.setDocumentLists(documentLists);
        dossierPharmacie = dossierPharmacieRepository.save(dossierPharmacie);
        documentRepository.deleteById(document.getId());
        storageService.deleteStoredFiles(document);
    }
}
