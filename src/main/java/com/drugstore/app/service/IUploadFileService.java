package com.drugstore.app.service;

import com.drugstore.app.domain.Document;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ibakrim
 *
 */
@Service
public interface IUploadFileService {
    public Document addUploadedFile(MultipartFile file);

    public Document save(Document document);
}
