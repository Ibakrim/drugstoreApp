package com.drugstore.app.service;

import com.drugstore.app.domain.Document;
import com.drugstore.app.domain.DossierPharmacie;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface IDossierPharmacieService {
    public DossierPharmacie createDossierPharmacieWithFiles(DossierPharmacie dossierPharmacie, MultipartFile[] files);

    public DossierPharmacie updateDossierPharmacieWithFiles(
        Long dossierPharmacieId,
        DossierPharmacie dossierPharmacie,
        MultipartFile[] files
    );

    public void handleFileDelete(DossierPharmacie dossierPharmacie, Document document);
}
