package com.drugstore.app.storage.service;

import com.drugstore.app.domain.Document;
import java.nio.file.Path;

public interface IThumbnailGenerationService {
    void init();

    Path generatePictureThumbnail(Document file);

    Path generatePdfThumbnail(Document file);

    void cleanTempThumbnail(Path tempThumbnailPath);

    void cleanTempImageAndThumbnail(Path tempThumbnailPath);
}
