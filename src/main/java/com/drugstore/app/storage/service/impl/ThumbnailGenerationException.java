package com.drugstore.app.storage.service.impl;

public class ThumbnailGenerationException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public ThumbnailGenerationException(String message) {
        super(message);
    }

    public ThumbnailGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}
