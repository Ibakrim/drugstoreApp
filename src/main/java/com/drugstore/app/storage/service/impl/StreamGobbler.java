package com.drugstore.app.storage.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class StreamGobbler implements Runnable {

    private InputStream inputStream;
    private Consumer<String> consumer;
    private InputStream errorStream;
    private Consumer<String> errorConsumer;

    public StreamGobbler(InputStream inputStream, Consumer<String> consumer, InputStream errorStream, Consumer<String> errorConsumer) {
        this.inputStream = inputStream;
        this.consumer = consumer;
        this.errorStream = errorStream;
        this.errorConsumer = errorConsumer;
    }

    @Override
    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(consumer);
        new BufferedReader(new InputStreamReader(errorStream)).lines().forEach(errorConsumer);
    }
}
