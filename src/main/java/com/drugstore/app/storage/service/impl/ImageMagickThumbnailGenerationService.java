package com.drugstore.app.storage.service.impl;

import com.drugstore.app.domain.Document;
import com.drugstore.app.storage.config.AppUploadedFileConfig;
import com.drugstore.app.storage.service.IStorageService;
import com.drugstore.app.storage.service.IThumbnailGenerationService;
import com.drugstore.app.utils.DrugStoreConstants;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageMagickThumbnailGenerationService implements IThumbnailGenerationService {

    private final Path tempDirectory;

    private final Path executable;

    private final Integer thumbnailSize;

    private static Logger logger = LoggerFactory.getLogger(ImageMagickThumbnailGenerationService.class);

    @Autowired
    IStorageService storageService;

    @Autowired
    public ImageMagickThumbnailGenerationService(AppUploadedFileConfig properties) {
        this.tempDirectory = Paths.get(properties.getThumbnailConversionTempDirectory());
        this.executable = Paths.get(properties.getMagickPath());
        this.thumbnailSize = Integer.parseInt(properties.getThumbnailSize());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(tempDirectory);
        } catch (IOException e) {
            logger.error("Could not initialize temporary thumbnail conversion directory", e);
            throw new ThumbnailGenerationException("Could not initialize temporary thumbnail conversion directory", e);
        }
    }

    public Path generateFirstPageAsPNG(String originPath) {
        try {
            Path sourcePath = storageService.load(originPath);
            PDDocument document = PDDocument.load(sourcePath.toFile());
            PDFRenderer pdfRenderer = new PDFRenderer(document);

            BufferedImage bim = pdfRenderer.renderImageWithDPI(0, DrugStoreConstants.IMAGE_DPI, ImageType.RGB);

            // suffix in filename will be used as the file format
            Path tempImageName = Paths.get(originPath.toString().replace(".pdf", ".png"));
            Path tempImagePath = tempDirectory.resolve(tempImageName);

            Files.createDirectories(tempImagePath.getParent());

            ImageIOUtil.writeImage(bim, tempImagePath.toString(), DrugStoreConstants.IMAGE_DPI);

            document.close();

            return tempImagePath;
        } catch (InvalidPasswordException e) {
            logger.error("Provided PDF was password-protected");
            throw new ThumbnailGenerationException("Provided PDF was password-protected");
        } catch (IOException e) {
            logger.error("An error occurred white converting the provided PDF to a PNG", e);
            throw new ThumbnailGenerationException("An error occurred white converting the provided PDF to a PNG", e);
        }
    }

    @Override
    public Path generatePdfThumbnail(Document file) {
        Path intermediatePicturePath = generateFirstPageAsPNG(file.getPath());

        return generateThumbnail(file, intermediatePicturePath);
    }

    @Override
    public Path generatePictureThumbnail(Document file) {
        Path sourcePath = storageService.load(file.getPath());

        return generateThumbnail(file, sourcePath);
    }

    private Path generateThumbnail(Document file, Path sourcePath) {
        Path tempThumbnailName = Paths.get(file.getPath() + "_thumb.png");
        Path tempThumbnailPath = tempDirectory.resolve(tempThumbnailName);

        try {
            Files.createDirectories(tempThumbnailPath.getParent());
        } catch (IOException e) {
            logger.error("Could not create temporary thumbnail directory", e);
            throw new ThumbnailGenerationException("Could not create temporary thumbnail directory", e);
        }

        String command = String.format(
            "%s convert -thumbnail %dx%d^ -gravity center -extent %dx%d -background white -alpha remove \"%s\" \"%s\"",
            executable.toString(),
            thumbnailSize,
            thumbnailSize,
            thumbnailSize,
            thumbnailSize,
            sourcePath.toAbsolutePath(),
            tempThumbnailPath.toAbsolutePath()
        );
        int returnCode = runCommand(command);

        if (returnCode != 0) {
            logger.error("Convert command failed");
        }

        return tempThumbnailName;
    }

    private Integer runCommand(String command) {
        boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");

        ProcessBuilder builder = new ProcessBuilder();
        if (isWindows) {
            builder.command("cmd.exe", "/c", command);
        } else {
            builder.command("sh", "-c", command);
        }
        builder.directory(new File(System.getProperty("user.home")));
        Process process;
        try {
            process = builder.start();
            StreamGobbler streamGobbler = new StreamGobbler(
                process.getInputStream(),
                logger::info,
                process.getErrorStream(),
                logger::error
            );

            Executors.newSingleThreadExecutor().submit(streamGobbler);
            int exitCode = process.waitFor();

            return exitCode;
        } catch (IOException e) {
            logger.error("Could not start thumbnail generation process", e);
            throw new ThumbnailGenerationException("Could not start thumbnail generation process", e);
        } catch (InterruptedException e) {
            logger.error("Thumbnail generation process was interrupted", e);
            throw new ThumbnailGenerationException("Thumbnail generation process was interrupted", e);
        }
    }

    @Override
    public void cleanTempImageAndThumbnail(Path tempThumbnailFile) {
        try {
            Files.delete(tempDirectory.resolve(tempThumbnailFile));
            Files.delete(tempDirectory.resolve(tempThumbnailFile.toString().replace(".pdf_thumb.png", ".png")));
        } catch (IOException e) {
            logger.error("Could not remove temporary thumbnail at location " + tempThumbnailFile.toString(), e);
        }
    }

    @Override
    public void cleanTempThumbnail(Path tempThumbnailFile) {
        try {
            Files.delete(tempDirectory.resolve(tempThumbnailFile));
        } catch (IOException e) {
            logger.error("Could not remove temporary thumbnail at location " + tempThumbnailFile.toString(), e);
        }
    }
}
