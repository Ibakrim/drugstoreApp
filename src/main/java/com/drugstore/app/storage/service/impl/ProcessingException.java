/**
 *
 */
package com.drugstore.app.storage.service.impl;

/**
 * @author ibakrim
 *
 */
public class ProcessingException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -7916806633891982068L;

    public ProcessingException(String message) {
        super(message);
    }

    public ProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
