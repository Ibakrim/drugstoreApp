/**
 *
 */
package com.drugstore.app.storage.service.impl;

/**
 * @author melgarhioui
 *
 */
public class ZipProvidedDocumentException extends RuntimeException {

    private static final long serialVersionUID = -1638972663447625770L;

    public ZipProvidedDocumentException(String message) {
        super(message);
    }

    public ZipProvidedDocumentException(String message, Throwable cause) {
        super(message, cause);
    }
}
