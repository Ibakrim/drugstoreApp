package com.drugstore.app.storage.service.impl;

import com.drugstore.app.domain.Document;
import com.drugstore.app.storage.config.AppUploadedFileConfig;
import com.drugstore.app.storage.service.IStorageService;
import com.drugstore.app.utils.DrugStoreConstants;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileSystemStorageService implements IStorageService {

    private final Logger LOG = LoggerFactory.getLogger(FileSystemStorageService.class);

    @Autowired
    private AppUploadedFileConfig appUploadConfig;

    private final Path rootLocation;

    private static Logger logger = LoggerFactory.getLogger(FileSystemStorageService.class);

    @Autowired
    public FileSystemStorageService(AppUploadedFileConfig properties) {
        this.rootLocation = Paths.get(properties.getUploadDirectory());
    }

    @Override
    public String store(MultipartFile file) {
        String ext = getExtensionByStringHandling(file.getOriginalFilename()).get();
        byte[] byteArr = null;
        try {
            byteArr = file.getBytes();
            if (byteArr.length == 0) {
                logger.error("Failed to store empty array");
                throw new StorageException("Failed to store empty array " + file.getName());
            }
        } catch (IOException e) {
            logger.error("Failed to convert file " + file.getName() + " to byte stream", e);
        }
        Optional<String> path = this.store(byteArr, ext);
        if (path.isPresent()) {
            return path.get();
        } else {
            throw new ProcessingException("An Exception occured during file storage");
        }
    }

    @Override
    public String storeZip(MultipartFile... fileTab) {
        ZipOutputStream zipOutputStream = null;
        try {
            Files.createDirectories(rootLocation);

            String ext = DrugStoreConstants.ZIP_EXTENSION;
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String uuid = generateUUID();
            String directory =
                String.valueOf(calendar.get(Calendar.YEAR)) +
                File.separator +
                String.valueOf(calendar.get(Calendar.MONTH)) +
                File.separator +
                String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            String filename = uuid + ext;
            Path rootLocation = Paths.get(appUploadConfig.getUploadDirectory() + directory + File.separator);

            String path = directory + File.separator + filename;

            Files.createDirectories(rootLocation);
            zipOutputStream = new ZipOutputStream(new FileOutputStream(rootLocation.resolve(filename).toFile()));
            int index = 0;
            for (MultipartFile item : fileTab) {
                zipOutputStream.putNextEntry(new ZipEntry((index++) + "-" + item.getOriginalFilename()));
                InputStream fileInputStream = item.getInputStream();
                IOUtils.copy(fileInputStream, zipOutputStream);
                fileInputStream.close();
                zipOutputStream.closeEntry();
            }

            return path;
        } catch (IOException e) {
            LOG.error("An error occured while zipping uploaded document.", e);
            throw new ZipProvidedDocumentException("An error occured while zipping uploaded document.");
        } finally {
            if (zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                } catch (IOException e) {
                    LOG.error("An error occured while trying to close zipOutputStream for uploaded document.", e);
                    throw new ZipProvidedDocumentException("An error occured while trying to close zipOutputStream for uploaded document.");
                }
            }
        }
    }

    @Override
    public String storeThumbnail(Document file, Path thumbnailPath) {
        Path currentPath = Paths.get(appUploadConfig.getThumbnailConversionTempDirectory()).resolve(thumbnailPath);
        Path finalPath = Paths.get(appUploadConfig.getUploadDirectory() + file.getPath() + "_thumb.png");

        try {
            FileInputStream stream = new FileInputStream(currentPath.toFile());
            Files.copy(stream, finalPath);
            stream.close();
        } catch (IOException e) {
            logger.error("Failed to store file" + finalPath, e);
            throw new ProcessingException("An error occured while storing file" + finalPath, e);
        }

        return thumbnailPath.toString();
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteFile(String pathFile) {
        File fileToDelete = new File(appUploadConfig.getUploadDirectory() + pathFile);
        try {
            if (pathFile.contains("..")) {
                // This is a security check
                throw new StorageException("Cannot delete file with relative path outside current directory " + fileToDelete.getName());
            } else if (fileToDelete.isFile()) {
                fileToDelete.delete();
            } else {
                throw new StorageException("directory can't be deleted " + fileToDelete.getName());
            }
        } catch (Exception e) {
            logger.error("Failed to delete file" + pathFile, e);
            throw new StorageException("Failed to delete file " + pathFile, e);
        }
    }

    @Override
    public void deleteStoredFiles(Document uploadedFile) {
        if (uploadedFile != null) {
            if (uploadedFile.getPath() != null) {
                deleteFile(uploadedFile.getPath());
            }
            if (uploadedFile.getThumbnailPath() != null) {
                deleteFile(uploadedFile.getThumbnailPath());
            }
        }
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    private String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    @Override
    public String storeProfileImage(MultipartFile file) {
        String ext = getExtensionByStringHandling(file.getOriginalFilename()).get();
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String uuid = generateUUID();
        String directory =
            String.valueOf("profile") +
            File.separator +
            String.valueOf("image") +
            File.separator +
            String.valueOf(calendar.get(Calendar.YEAR)) +
            File.separator +
            String.valueOf(calendar.get(Calendar.MONTH)) +
            File.separator +
            String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String filename = uuid + "." + ext;
        Path rootLocation = Paths.get(appUploadConfig.getUploadDirectory() + directory + File.separator);

        String path = directory + File.separator + filename;

        try {
            Files.createDirectories(rootLocation);

            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            } else if (filename.contains("..")) {
                // This is a security check
                throw new StorageException("Cannot store file with relative path outside current directory " + filename);
            } else if (appUploadConfig.getExtValid().split(ext + ";").length != 2) {
                // This is a security check
                throw new StorageException("Cannot store file extension " + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            logger.error("Failed to store file" + filename, e);
            throw new StorageException("Failed to store file " + filename, e);
        }

        return path;
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename).filter(f -> f.contains(".")).map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    @Override
    public Optional<String> store(byte[] byteArray, String ext) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String uuid = generateUUID();
        String directory =
            String.valueOf(calendar.get(Calendar.YEAR)) +
            File.separator +
            String.valueOf(calendar.get(Calendar.MONTH)) +
            File.separator +
            String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String filename = uuid + "." + ext;
        Path rootLocation = Paths.get(appUploadConfig.getUploadDirectory() + directory + File.separator);

        String path = directory + File.separator + filename;

        try {
            Files.createDirectories(rootLocation);

            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException("Cannot store file with relative path outside current directory " + filename);
            } else if (appUploadConfig.getExtValid().split(ext + ";").length != 2) {
                // This is a security check
                throw new StorageException("Cannot store file extension " + filename);
            }
            try (InputStream inputStream = new ByteArrayInputStream(byteArray);) {
                Files.copy(inputStream, rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            logger.error("Failed to store file" + filename, e);
            throw new ProcessingException("An error occured while storing file" + filename, e);
        }

        return Optional.of(path);
    }
}
