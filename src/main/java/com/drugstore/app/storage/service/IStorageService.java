package com.drugstore.app.storage.service;

import com.drugstore.app.domain.Document;
import java.nio.file.Path;
import java.util.Optional;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IStorageService {
    void init();

    String store(MultipartFile file);

    String storeZip(MultipartFile... file);

    String storeThumbnail(Document baseFile, Path tempThumbnailPath);

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteFile(String path);

    public String storeProfileImage(MultipartFile file);

    public Optional<String> store(byte[] byteArray, String originalFilename);

    public void deleteStoredFiles(Document uploadedFile);
}
