package com.drugstore.app.storage.service.impl;

public class StorageFileNotFoundException extends StorageException {

    static final long serialVersionUID = 2L;

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
