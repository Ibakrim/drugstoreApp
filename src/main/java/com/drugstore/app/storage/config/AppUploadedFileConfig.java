package com.drugstore.app.storage.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:uploadedFile.properties")
public class AppUploadedFileConfig {

    @Value("${pharma.common.uploadDirectory}")
    private String uploadDirectory;

    @Value("${pharma.common.extValid}")
    private String extValid;

    @Value("${cri.common.thumbnails.magickPath}")
    private String magickPath;

    @Value("${cri.common.thumbnails.size}")
    private String thumbnailSize;

    @Value("${cri.common.thumbnails.tempDirectory}")
    private String thumbnailConversionTempDirectory;

    @Value("${pharma.common.downloadDirectory}")
    private String downloadDirectory;

    public String getExtValid() {
        return extValid;
    }

    public void setExtValid(String extValid) {
        this.extValid = extValid;
    }

    public String getUploadDirectory() {
        return uploadDirectory;
    }

    public void setUploadDirectory(String uploadDirectory) {
        this.uploadDirectory = uploadDirectory;
    }

    public AppUploadedFileConfig() {}

    /**
     * @return the magickPath
     */
    public String getMagickPath() {
        return magickPath;
    }

    /**
     * @param magickPath the magickPath to set
     */
    public void setMagickPath(String magickPath) {
        this.magickPath = magickPath;
    }

    /**
     * @return the thumbnailSize
     */
    public String getThumbnailSize() {
        return thumbnailSize;
    }

    /**
     * @param thumbnailSize the thumbnailSize to set
     */
    public void setThumbnailSize(String thumbnailSize) {
        this.thumbnailSize = thumbnailSize;
    }

    /**
     * @return the thumbnailConversionTempDirectory
     */
    public String getThumbnailConversionTempDirectory() {
        return thumbnailConversionTempDirectory;
    }

    /**
     * @param thumbnailConversionTempDirectory the thumbnailConversionTempDirectory to set
     */
    public void setThumbnailConversionTempDirectory(String thumbnailConversionTempDirectory) {
        this.thumbnailConversionTempDirectory = thumbnailConversionTempDirectory;
    }

    public String getDownloadDirectory() {
        return downloadDirectory;
    }

    public void setDownloadDirectory(String downloadDirectory) {
        this.downloadDirectory = downloadDirectory;
    }
}
