package com.drugstore.app.storage.config;

import com.drugstore.app.utils.DrugStoreUtils;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${drugstore.base.url}")
    private String baseURL;

    @Value("${drugstore.support.base.url}")
    private String supportBaseURL;

    @Value("${drugstore.server.servlet.contextPath}")
    private String contextPath;

    public String getSupportBaseURL() {
        return supportBaseURL;
    }

    public void setSupportBaseURL(String supportBaseURL) {
        this.supportBaseURL = supportBaseURL;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    @PostConstruct
    private void init() {
        DrugStoreUtils.setAppConfig(this);
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }
}
